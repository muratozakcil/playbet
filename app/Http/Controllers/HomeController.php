<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Partners;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Rules\Captcha;

class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }
    public function partner_payment()
    {
        $payment_partners = Partners::where('tur','payment')->get();
        return view('payment_partner',compact('payment_partners'));
    }
    public function partner_gaming()
    {
        $gaming_partners = Partners::where('tur','gaming')->get();
        return view('gaming_partner',compact('gaming_partners'));
    }
    public function success()
    {
        return view('success');
    }

    public function blog_detay($slug)
    {
        $detay = Blog::where('slug',$slug)->first();

        return view('blog', compact('detay'));
    }
    public function iletisim(Request $request)
    {
        return view('contact');
    }
    public function iletisim_general(Request $request)
    {
        $this->validate($request,[
            'g-recaptcha' => new Captcha(),
        ]);
        $data = [
            'names' => $request->name,
            'phone_numbers' => $request->phone,
            'emails' => $request->email,
            'skypes' => $request->skype,
            'whatsapps' => $request->whatsapp,
            'interesteds' => $request->interested,
            'messages' => $request->message
        ];

        $email2 = "info@playbetic.com";

        Mail::send('layouts.partials.message_general', $data, function ($message) use ($email2) {
            $message->to($email2)
                ->subject('Contact - PlayBetic')
                ->from('info@playbetic.com', 'Contact - PlayBetic');
        });

        return redirect()->route('success');
    }
    public function iletisim_sales(Request $request)
    {
        $this->validate($request,[
            'g-recaptcha' => new Captcha(),
        ]);
        $data = [
            'company' => $request->company,
            'website' => $request->website,
            'cperson' => $request->cperson,
            'phone_number' => $request->phone_number,
            'position' => $request->position,
            'email' => $request->email,
            'country' => $request->country,
            'language' => $request->language,
            'license' => $request->license,
            'currencies' => $request->currencies,
            'sportsbook' => $request->sportsbook,
            'casino_provid' => $request->casino_provid,
            'message' => $request->message
        ];

        $email2 = "info@playbetic.com";

        Mail::send('layouts.partials.message', $data, function ($message) use ($email2) {
            $message->to($email2)
                ->subject('Contact - PlayBetic')
                ->from('info@playbetic.com', 'Contact - PlayBetic');
        });

        return redirect()->route('success');
    }
}
