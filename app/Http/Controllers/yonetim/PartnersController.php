<?php

namespace App\Http\Controllers\yonetim;

use App\Models\Partners;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnersController extends Controller
{
    public function index()
    {
        $gaming_partners = Partners::where('tur','gaming')->get();
        $payment_partners = Partners::where('tur','payment')->get();
        return view('yonetim.partner.index', compact('gaming_partners','payment_partners'));
    }

    public function form($id = 0)
    {
        $entry = new Partners();
        if ($id>0)
        {
            $entry = Partners::find($id);
        }
        return view('yonetim.partner.form', compact('entry'));
    }

    public function kaydet($id = 0, Request $request)
    {
        $tur = $request->tur;
        if($request->hasfile('image'))
        {
            foreach($request->file('image') as $image)
            {
                $name= $image->getClientOriginalName();
                $image->move('partners/', $name);
                Partners::create([
                    'tur' => $tur,
                    'image' => $name
                ]);
            }
            return redirect()->route('yonetim.partners')
                ->with('mesaj', 'Başarılı birşekilde eklendi')
                ->with('mesaj_tur', 'success');
        }else{
            return redirect()->route('yonetim.partners')
                ->with('mesaj', 'Resim eklenirken hata oluştu')
                ->with('mesaj_tur', 'danger');
        }
    }
    public function delete($id)
    {
        $kategori = Partners::find($id);
        $kategori->delete();
        return redirect()
            ->back()
            ->with('mesaj','Silme İşlemi Başarılı')
            ->with('mesaj_tur','success');
    }
}
