<?php

namespace App\Http\Controllers\yonetim;

use App\Models\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index()
    {
        $bloglar = Blog::orderBy('id','desc')->get();
        return view('yonetim.blog.index', compact('bloglar'));
    }
    public function form($id = 0)
    {
        $entry = new Blog();
        if ($id>0)
        {
            $entry = Blog::find($id);

        }
        return view('yonetim.blog.form', compact('entry'));
    }
    public function kaydet($id = 0, Request $request)
    {

        $data = request()->only('baslik','kisa_aciklama','keywords','aciklama');
        $data['slug'] = str_slug(request('baslik'));
        $this->validate(request(),[
            'baslik' => 'required',
            'kisa_aciklama' => 'required',
            'keywords' => 'required',
            'aciklama' => 'required'
        ]);
        if($request->hasfile('resim')) {
            $one_cikan = request()->file('resim');
            $one_cikan = request()->resim;
            $one_cikan_name = time() . "." . $one_cikan->extension();
            $one_cikan->move('bloglar/', $one_cikan_name);
            $data['resim'] = $one_cikan_name;
        }

        if ($id>0)
        {
            $entry = Blog::where('id', $id)->firstOrFail();
            $entry->update($data);

        }else{
            $entry = Blog::create($data);
        }

        return redirect()->route('yonetim.blog')
            ->with('mesaj', ($id>0 ? 'Güncellendi' : 'Kaydedildi'))
            ->with('mesaj_tur', 'success');
    }
    public function delete($id)
    {
        Blog::destroy($id);
        return redirect()
            ->back()
            ->with('mesaj','Silme İşlemi Başarılı')
            ->with('mesaj_tur','success');
    }
}
