<?php

namespace App\Http\Controllers\yonetim;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function yonetim()
    {
        if (request()->isMethod('POST'))
        {
            $this->validate(request(),[
                'email' => 'required|email',
                'sifre' => 'required'
            ]);
            if (auth()->attempt(['email' => request('email'), 'password' => request('sifre'), 'aktif_mi'=> 1]))
            {
                request()->session()->regenerate();
                return redirect()->route('yonetim.home');
            }else {
                $errors = ['email' => 'Hatalı giriş'];
                return back()->withErrors($errors);
            }

        }
        if (Auth::guest()) {
            /*return Hash::make('play788**');*/
            return view('yonetim.oturumac');
        } else {
            return redirect()->route('yonetim.home');
        }

    }
    public function logout()
    {
        auth()->logout();
        request()->session()->flush();
        request()->session()->regenerate();
        return redirect()->route('login');
    }
}
