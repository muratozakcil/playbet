<?php

namespace App\Http\Controllers\yonetim;

use App\Models\Ayar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AyarController extends Controller
{
    public function index()
    {
        $entry = Ayar::where('id', 1)->firstOrFail();
        return view('yonetim.ayarlar.index', compact('entry'));
    }

    public function genelayarlar()
    {
        $data = request()->only(
            'site_title',
            'site_desc',
            'keywords',
            'site_skype',
            'site_telefon',
            'site_email',
            'hakkimizda'
        );

        $this->validate(request(),[
            'site_title' => 'required',
            'site_desc' => 'required',
            'keywords' => 'required'
        ]);

        $entry = Ayar::where('id', 1)->firstOrFail();
        $entry->update($data);


        return redirect()->route('yonetim.ayar')
            ->with('mesaj', 'Güncellendi')
            ->with('mesaj_tur', 'success');
    }
}
