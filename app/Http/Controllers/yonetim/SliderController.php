<?php

namespace App\Http\Controllers\yonetim;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::orderBy('id','desc')->get();
        return view('yonetim.slider.index', compact('sliders'));
    }

    public function form($id = 0)
    {
        $entry = new Slider();
        if ($id>0)
        {
            $entry = Slider::find($id);

        }
        return view('yonetim.slider.form', compact('entry'));
    }

    public function kaydet($id = 0, Request $request)
    {

        $data = request()->only('sira');
        $this->validate(request(),[
            'sira' => 'required'
        ]);
        if($request->hasfile('image')) {
            $one_cikan = request()->file('image');
            $one_cikan = request()->image;
            $one_cikan_name = time() . "." . $one_cikan->extension();
            $one_cikan->move('slider/', $one_cikan_name);
            $data['image'] = $one_cikan_name;
        }

        if ($id>0)
        {
            $entry = Slider::where('id', $id)->firstOrFail();
            $entry->update($data);

        }else{
            $entry = Slider::create($data);
        }

        return redirect()->route('yonetim.slider')
            ->with('mesaj', ($id>0 ? 'Güncellendi' : 'Kaydedildi'))
            ->with('mesaj_tur', 'success');
    }
    public function delete($id)
    {
        Slider::destroy($id);
        return redirect()
            ->back()
            ->with('mesaj','Silme İşlemi Başarılı')
            ->with('mesaj_tur','success');
    }
}
