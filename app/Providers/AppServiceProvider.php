<?php

namespace App\Providers;

use App\Models\Ayar;
use App\Models\Blog;
use App\Models\Slider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::composer(['yonetim.anasayfa'], function ($views){
            $kontrol_paneli =
                [
                    'blog' => Blog::count(),
                ];

            $views->with('kontrol_paneli', $kontrol_paneli);
        });
        View::composer(['*'], function ($views){
            $metakod =
                [
                    'blog' => Blog::take(3)->get(),
                    'slider' => Slider::orderBy('sira','desc')->get(),
                    'ayar' => Ayar::where('id',1)->first()
                ];

            $views->with('ayarlar', $metakod);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
