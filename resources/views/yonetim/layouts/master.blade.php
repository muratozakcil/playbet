<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>@yield('title', config('app.name'))</title>
    @include('yonetim.layouts.partials.head')
    @yield('head')
</head>
<body>
<div class="se-pre-con"></div>
<div id="app">
    <div class="main-wrapper main-wrapper-1">
        @include('yonetim.layouts.partials.navbar')
        @include('yonetim.layouts.partials.sidebar')
      @yield('content')
</div>
</div>
@include('yonetim.layouts.partials.footer')
@yield('footer')
