<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('yonetim.home') }}">Yönetim Paneli</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('yonetim.home') }}">YP</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Kontrol Paneli</li>
            <li class="{{ (request()->is('yonetim')) ? 'active' : '' }}"><a class="nav-link" href="{{ route('yonetim.home') }}"><i class="fas fa-fire"></i> <span>Anasayfa</span></a></li>
            <li class="menu-header">Blog Yönetimi</li>
            <li class="{{ (request()->is('yonetim/blog')) ? 'active' : '' }}"><a class="nav-link" href="{{ route('yonetim.blog') }}"><i class="far fa-file-alt"></i> <span>Bloglar</span></a></li>
            <li class="menu-header">Slider Yönetimi</li>
            <li class="{{ (request()->is('yonetim/slider')) ? 'active' : '' }}"><a class="nav-link" href="{{ route('yonetim.slider') }}"><i class="far fa-file-alt"></i> <span>Sliderlar</span></a></li>
            <li class="menu-header">Partners Yönetimi</li>
            <li class="{{ (request()->is('yonetim/partners')) ? 'active' : '' }}"><a class="nav-link" href="{{ route('yonetim.partners') }}"><i class="far fa-file-alt"></i> <span>Partners</span></a></li>

        </ul>

        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="{{ route('yonetim.ayar') }}" class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-cog"></i> Ayarlar
            </a>
        </div>
    </aside>
</div>
