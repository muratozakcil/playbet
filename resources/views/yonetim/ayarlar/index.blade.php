@extends('yonetim.layouts.master')
@section('title','Ayarlar')
@section('head')
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/summernote/summernote-bs4.css') }}">
@endsection
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Site Ayarları</h1>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-12">
                        @include('layouts.partials.errors')
                        @include('layouts.partials.alert')
                        <div class="card">
                            <div class="card-body">
                                <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="genelayarlar-tab4" data-toggle="tab" href="#genelayarlar4" role="tab" aria-controls="genelayarlar" aria-selected="true">Genel Ayarlar</a>
                                    </li>
                                </ul>
                                <div class="tab-content tab-bordered" id="myTab2Content">
                                    <div class="tab-pane fade show active" id="genelayarlar4" role="tabpanel" aria-labelledby="genelayarlar-tab4">
                                        <div class="section-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <form method="post" action="{{ route('yonetim.ayar.genelayarlar') }}">
                                                                {{ csrf_field() }}
                                                                <div class="form-group row mb-4">
                                                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Site Title *</label>
                                                                    <div class="col-sm-12 col-md-7">
                                                                        <input type="text" class="form-control" name="site_title" id="site_title" value="{{ old('site_title', $entry->site_title) }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-4">
                                                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Site Anahtar Kelimeler *</label>
                                                                    <div class="col-sm-12 col-md-7">
                                                                        <textarea name="keywords" class="form-control">{{ old('keywords', $entry->keywords) }}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-4">
                                                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Site Açıklama *</label>
                                                                    <div class="col-sm-12 col-md-7">
                                                                        <textarea class="form-control" name="site_desc" >{{ old('site_desc', $entry->site_desc) }}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-4">
                                                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Site Telefon *</label>
                                                                    <div class="col-sm-12 col-md-7">
                                                                        <input type="text" class="form-control" name="site_telefon" id="site_telefon" value="{{ old('site_telefon', $entry->site_telefon) }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-4">
                                                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Site E-posta *</label>
                                                                    <div class="col-sm-12 col-md-7">
                                                                        <input type="email" class="form-control" name="site_email" id="site_email" value="{{ old('site_email', $entry->site_email) }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-4">
                                                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Site Skype *</label>
                                                                    <div class="col-sm-12 col-md-7">
                                                                        <input type="text" class="form-control" name="site_skype" id="site_skype" value="{{ old('site_skype', $entry->site_skype) }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-4">
                                                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Hakkımızda Yazı *</label>
                                                                    <div class="col-sm-12 col-md-7">
                                                                        <textarea style="width: 100%;" name="hakkimizda" >{{ old('hakkimizda', $entry->hakkimizda) }}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row mb-4">
                                                                    <div class="col-sm-12 col-md-12">
                                                                        <button style="float: right;" type="submit" class="btn btn-icon icon-left btn-success"><i class="fas fa-check"></i> Güncelle</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer')
    <script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
@endsection
