@extends('yonetim.layouts.master')
@section('title','Anasayfa')
@section('head')
@endsection
@section('content')
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>Kontrol Paneli</h1>
                    </div>

                    <div class="section-body">
                        <div class="row">

                            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="card card-statistic-1">
                                    <div class="card-icon bg-warning">
                                        <i class="far fa-file"></i>
                                    </div>
                                    <div class="card-wrap">
                                        <div class="card-header">
                                            <h4>Toplam Blog</h4>
                                        </div>
                                        <div class="card-body">
                                            {{ $kontrol_paneli['blog'] }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
@endsection
