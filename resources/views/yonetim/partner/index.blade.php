@extends('yonetim.layouts.master')
@section('title','Partners')
@section('head')
    <link rel="stylesheet" href="{{ asset('assets/modules/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/codemirror/lib/codemirror.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/codemirror/theme/duotone-dark.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jquery-selectric/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/chocolat/dist/css/chocolat.css') }}">
    <style>
        .galeri_resim{
            width: 150px!important;
            height: 150px!important;
            float: left;
            margin-right: 10px;
            margin-bottom: 10px;
        }
        .galeri_resim img{
            width: 150px!important;
            height: 150px!important;
            transition: all 0.5s;
            object-fit: cover;
        }
        .chocolat-wrapper{
            z-index: 999;
        }
        #galeri_delete{
            position: absolute;
            top: 90px;
        }
    </style>
@endsection
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Partners</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('yonetim.home') }}">Kontrol Paneli</a></div>
                    <div class="breadcrumb-item"><a href="{{ route('yonetim.partners') }}">Partners</a></div>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        @include('layouts.partials.errors')
                        @include('layouts.partials.alert')
                        <div class="card">
                            <div class="card-body">
                                <form method="post" action="{{ route('yonetim.partners.kaydet') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Resim Toplu Seçilebilir</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" class="form-control" name="image[]" multiple />
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tür</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="form-control" name="tur">
                                                <option value="payment">Payment Partners</option>
                                                <option value="gaming">Gaming Partners</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <div class="col-sm-12 col-md-12">
                                            <button style="float: right;" type="submit" class="btn btn-icon icon-left btn-success"><i class="fas fa-check"></i> Ekle</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="col-12 col-sm-6 col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>Partner Gaming</h4>
                                        </div>
                                        <div class="card-body">
                                            <div id="galeri" class="gallery gallery-md">
                                                @foreach($gaming_partners as $galeriler)
                                                    <div class="galeri_resim">
                                                        <img alt="image" src="{{ asset('partners'.'/'.$galeriler->image) }}" data-toggle="tooltip" title="" data-original-title="Alfa Zulkarnain">
                                                        <a id="galeri_delete" href="{{ route('yonetim.partners.delete', $galeriler->id) }}" class="btn btn-icon btn-danger"><i class="fas fa-times"></i></a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="col-12 col-sm-6 col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>Partner Payment</h4>
                                        </div>
                                        <div class="card-body">
                                            <div id="galeri" class="gallery gallery-md">
                                                @foreach($payment_partners as $galeriler)
                                                    <div class="galeri_resim">
                                                        <img alt="image" src="{{ asset('partners'.'/'.$galeriler->image) }}" data-toggle="tooltip" title="" data-original-title="Alfa Zulkarnain">
                                                        <a id="galeri_delete" href="{{ route('yonetim.partners.delete', $galeriler->id) }}" class="btn btn-icon btn-danger"><i class="fas fa-times"></i></a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer')
    <!-- JS Libraies -->
    <script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
    <script src="{{ asset('assets/modules/codemirror/lib/codemirror.js') }}"></script>
    <script src="{{ asset('assets/modules/codemirror/mode/javascript/javascript.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('assets/modules/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>
    <script>
        $(function () {
            var options = {
                filebrowserImageBrowseUrl: '{{ URL::asset('/laravel-filemanager?type=Images') }}',
                filebrowserImageUploadUrl: '{{ URL::asset('/laravel-filemanager/upload?type=Images&_token=') }}',
                filebrowserBrowseUrl: '{{ URL::asset('/laravel-filemanager?type=Files') }}',
                filebrowserUploadUrl: '{{ URL::asset('/laravel-filemanager/upload?type=Files&_token=') }}'
            };
            CKEDITOR.replace('aciklama' , options);
        })
    </script>
@endsection
