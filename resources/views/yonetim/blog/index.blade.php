@extends('yonetim.layouts.master')
@section('title','Bloglar')
@section('head')
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
@endsection
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Bloglar</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('yonetim.home') }}">Kontrol Paneli</a></div>
                    <div class="breadcrumb-item">Bloglar</div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        @include('layouts.partials.errors')
                        @include('layouts.partials.alert')
                        <div class="card">
                            <div class="card-body">
                                <a href="{{ route('yonetim.blog.yeni') }}" style="float: right;margin-bottom: 3%;" class="btn btn-success">Yeni Blog Ekle</a>
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                        <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Başlık</th>
                                            <th>Kayıt Tarihi</th>
                                            <th>İşlemler</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($bloglar as $key=>$blog)
                                            <tr>
                                                <td>
                                                   {{ $key+1 }}
                                                </td>
                                                <td>{{ $blog->baslik }}</td>
                                                <td>{{ $blog->created_at }}</td>
                                                <td>
                                                    <a href="{{ route('yonetim.blog.duzenle', $blog->id) }}" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a>
                                                    <a href="{{ route('yonetim.blog.delete', $blog->id) }}" class="btn btn-icon btn-danger"><i class="fas fa-times"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer')
    <!-- JS Libraies -->
    <script src="{{ asset('assets/modules/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- Page Specific JS File -->
    <script src="{{ asset('assets/js/page/modules-datatables.js') }}"></script>
    @endsection
