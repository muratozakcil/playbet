@extends('yonetim.layouts.master')
@section('title','Blog')
@section('head')
    <link rel="stylesheet" href="{{ asset('assets/modules/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/codemirror/lib/codemirror.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/codemirror/theme/duotone-dark.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/jquery-selectric/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/chocolat/dist/css/chocolat.css') }}">
    <style>
        .galeri_resim{
            width: 150px!important;
            height: 150px!important;
            float: left;
            margin-right: 10px;
            margin-bottom: 10px;
        }
        .galeri_resim img{
            width: 150px!important;
            height: 150px!important;
            transition: all 0.5s;
            object-fit: cover;
        }
        .chocolat-wrapper{
            z-index: 999;
        }
        #galeri_delete{
            position: absolute;
            top: 90px;
        }
    </style>
@endsection
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Blog {{ $entry->id > 0 ? "Düzenle" : "Ekle" }}</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('yonetim.home') }}">Kontrol Paneli</a></div>
                    <div class="breadcrumb-item"><a href="{{ route('yonetim.blog') }}">Bloglar</a></div>
                    <div class="breadcrumb-item">Blog {{ $entry->id > 0 ? "Düzenle" : "Ekle" }}</div>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        @include('layouts.partials.errors')
                        @include('layouts.partials.alert')
                        <div class="card">
                            <div class="card-body">
                                <form method="post" action="{{ route('yonetim.blog.kaydet', $entry->id) }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Başlık *</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control" name="baslik" id="baslik" value="{{ old('baslik', $entry->baslik) }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keywords</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea style="width: 100%;" name="keywords" spellcheck="false">{{ old('keywords', $entry->keywords) }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kısa Açıklama</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea style="width: 100%;" name="kisa_aciklama" spellcheck="false">{{ old('kisa_aciklama', $entry->kisa_aciklama) }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Önce Çıkan Resim</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" class="form-control" name="resim" />
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">İçerik</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea class="form-control" name="aciklama" id="aciklama" placeholder="İçerik">{{ old('aciklama', $entry->aciklama) }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <div class="col-sm-12 col-md-12">
                                            <button style="float: right;" type="submit" class="btn btn-icon icon-left btn-success"><i class="fas fa-check"></i> {{ $entry->id > 0 ? "Düzenle" : "Ekle" }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('footer')
    <!-- JS Libraies -->
    <script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
    <script src="{{ asset('assets/modules/codemirror/lib/codemirror.js') }}"></script>
    <script src="{{ asset('assets/modules/codemirror/mode/javascript/javascript.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('assets/modules/chocolat/dist/js/jquery.chocolat.min.js') }}"></script>
    <script>
        $(function () {
            var options = {
                filebrowserImageBrowseUrl: '{{ URL::asset('/laravel-filemanager?type=Images') }}',
                filebrowserImageUploadUrl: '{{ URL::asset('/laravel-filemanager/upload?type=Images&_token=') }}',
                filebrowserBrowseUrl: '{{ URL::asset('/laravel-filemanager?type=Files') }}',
                filebrowserUploadUrl: '{{ URL::asset('/laravel-filemanager/upload?type=Files&_token=') }}'
            };
            CKEDITOR.replace('aciklama' , options);
        })
    </script>
@endsection
