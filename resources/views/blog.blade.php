@extends('layouts.master')
@section('title',$detay->baslik)
@section('content')
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="cube-wrapper">
                <div class="cube-folding">
                    <span class="leaf1"></span>
                    <span class="leaf2"></span>
                    <span class="leaf3"></span>
                    <span class="leaf4"></span>
                </div>
                <span class="loading" data-name="Loading">Loading</span>
            </div>
        </div>
    </div>
    <header class="navigation">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 p-0">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/">
                            <img src="{{ asset('tema/assets/images/logo-color.png') }}" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainmenu"
                                aria-controls="mainmenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="mainmenu">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Sportsbook</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Virtuals</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Casino</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Lotto</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Blog</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <header class="breadcrumb-area ">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="title">
                        {{ $detay->baslik }}
                    </h2>
                    <ul class="links">
                        <li>
                            <a href="/">
                                <i class="fas fa-home"></i>
                                Home
                            </a>
                        </li>
                        <li>
                            <a class="active" href="#">
                                {{ $detay->baslik }}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="right-img">
                    <img src="{{ asset('tema/assets/images/breadcrumb.png') }}" alt="">
                </div>
            </div>
        </div>
    </header>
    <section class="blog-details" id="blog-details">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-content">
                        <div style="    text-align: center;" class="feature-image">
                            <img class="img-fluid" src="{{ asset('bloglar/'.$detay->resim) }}" alt="">
                        </div>
                        <div class="content">
                            <h3 class="title">
                                {{ $detay->baslik }}
                            </h3>
                            <ul class="post-meta">
                                <li>
                                    <a href="#">
                                        <i class="fas fa-user-tie"></i>
                                        <span>Admin</span>
                                    </a>
                                </li>
                            </ul>
                            <p>
                                {!! $detay->aciklama !!}
                            </p>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </section>
@endsection


