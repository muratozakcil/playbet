@extends('layouts.master')
@section('title','Playbetic')
@section('head')
    <style>
        #header
        {
            -webkit-box-shadow: 0 8px 6px -6px black;
            -moz-box-shadow: 0 8px 6px -6px black;
            box-shadow: 0 8px 6px -6px black;
        }
    </style>
@endsection
@section('content')
    <!-- About-Us -->
    <section id="solutions" style="padding: 160px 0"  class="section-padding">
        <div style="margin-bottom: 50px;" class="container">
            <div class="box-info">
                <h6 style="font-weight: 100;">@lang('th.gaming_partners')</h6>
                <p>@lang('th.gaming_partners_1')</p>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <ul class="partners list-unstyled mt-60">
                    @foreach($gaming_partners as $gaming_partner)
                    <li>
                        <a href="#sportsbetting" data-aos="fade-up" class="aos-init aos-animate wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('partners/'.$gaming_partner->image) }}"></div>
                        </a>
                    </li>
                    @endforeach
                    <li class="clearfix"></li>
                </ul>
            </div>
        </div>
    </section>
@endsection


