@extends('layouts.master')
@section('title','Playbetic')
@section('content')


    <!-- Banner -->
    <section id="banner" class="bannr_slideshow video_bg parallex-bg">
        <div class="owl-carousel owl-theme">

            @foreach($ayarlar['slider'] as $slider)
                <div class="slides masked">
                    <div class="intro_img">
                        <img src="{{ asset('slider/'.$slider->image) }}" alt="image">
                    </div>
                </div>

            @endforeach

        </div>
    </section>
    <section id="sportsbook" class="section-padding">
        <div class="container">
            <div class="row">
                <ul class="solutions list-unstyled mt-60">
                    <li>
                        <a href="#sportsbetting" data-aos="fade-up" class="aos-init aos-animate wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/sports-betting-ico.png') }}"></div>
                        </a>
                    </li>
                    <li>
                        <a href="#livesports" data-aos="fade-up" data-aos-delay="50" class="aos-init aos-animate wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/live-sport-ico.png') }}"></div>
                        </a>
                    </li>
                    <li>
                        <a href="#mobile" data-aos="fade-up" data-aos-delay="100" class="aos-init aos-animate wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/mobile-ico.png') }}"></div>
                        </a>
                    </li>
                    <li>
                        <a href="#retail" data-aos="fade-up" data-aos-delay="150" class="aos-init aos-animate wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/retail-ico.png') }}"></div>

                        </a>
                    </li>
                    <li>
                        <a href="#online" data-aos="fade-up" data-aos-delay="200" class="aos-init aos-animate wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/online-ico.png') }}"></div>

                        </a>
                    </li>
                    <li>
                        <a href="#casino" data-aos="fade-up" data-aos-delay="250" class="aos-init wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/casino-ico.png') }}"></div>

                        </a>
                    </li>
                    <li>
                        <a href="#virtualsport" data-aos="fade-up" data-aos-delay="300" class="aos-init wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/virtual-sports-ico.png') }}"></div>

                        </a>
                    </li>
                    <li>
                        <a href="#adminconsole" data-aos="fade-up" data-aos-delay="350" class="aos-init wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/adimn-console-ico.png') }}"></div>

                        </a>
                    </li>
                    <li>
                        <a href="#odds" data-aos="fade-up" data-aos-delay="400" class="aos-init wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/odds-data-service-ico.png') }}"></div>

                        </a>
                    </li>
                    <li>
                        <a href="#whitelabel" data-aos="fade-up" data-aos-delay="450" class="aos-init wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/white-label-ico.png') }}"></div>

                        </a>
                    </li>
                    <li id="mobil_hide">
                    </li>
                    <li>
                        <a href="#freecupon" data-aos="fade-up" data-aos-delay="500" class="aos-init wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/free-coupon-ico.png') }}"></div>

                        </a>
                    </li>
                    <li>
                        <a href="#freebet" data-aos="fade-up" data-aos-delay="550" class="aos-init wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/free-bet-ico.png') }}"></div>

                        </a>
                    </li>
                    <li>
                        <a href="#cashout" data-aos="fade-up" data-aos-delay="600" class="aos-init wow zoomIn">
                            <div class="solutions-icon"><img src="{{ asset('tema/assets/images/solutions/cashout-ico.png') }}"></div>

                        </a>
                    </li>
                    <li class="clearfix"></li>
                </ul>
            </div>
        </div>
    </section>
    <section id="sportsbetting" class="section-padding">
        <div style="border-bottom: 1px dashed #e30e0e;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.s_bettings')</h3>
                </div>
                <div class="col-md-5 text-right wow fadeInLeft">
                    <img src="{{ asset('tema/assets/images/001.jpg') }}" alt="image">
                </div>
                <div class="col-md-6 wow fadeInRight">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/sports-001.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.s_bettings')</h6>
                            <ul>
                                <li style="list-style: none;" id="sports">@lang('th.sb_1')</li>
                                <li style="list-style: none;" id="sports">@lang('th.sb_2')</li>
                                <li style="list-style: none;" id="sports">@lang('th.sb_3')</li>
                                <li style="list-style: none;" id="sports">@lang('th.sb_4')</li>
                                <li style="list-style: none;" id="sports">@lang('th.sb_5')</li>
                                <li style="list-style: none;" id="sports">@lang('th.sb_6')</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section id="livesports" class="section-padding">
        <div  class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.l_sport')</h3>
                </div>
                <div class="col-md-6 wow fadeInLeft">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/live-sports.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.l_sport')</h6>
                            <ul>
                                <li style="list-style: none;" id="livesport">@lang('th.ls_1')</li>
                                <li style="list-style: none;" id="livesport">@lang('th.ls_2')</li>
                                <li style="list-style: none;" id="livesport">@lang('th.ls_3')</li>
                                <li style="list-style: none;" id="livesport">@lang('th.ls_4')</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 text-right wow fadeInRight">
                    <img src="{{ asset('tema/assets/images/live-sports-001.png') }}" alt="image">
                </div>


            </div>
        </div>
    </section>
    <section style="background-color: #cdcdcd;" id="retail" class="section-padding">
        <div  class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.retail')</h3>
                </div>
                <div class="col-md-6 wow fadeInLeft">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/retail.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.retail')</h6>
                            <ul>
                                <li style="list-style: none;" id="retails">@lang('th.retail_1')</li>
                                <li style="list-style: none;" id="retails">@lang('th.retail_2')</li>
                                <li style="list-style: none;" id="retails">@lang('th.retail_3')</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 text-right wow fadeInRight">
                    <img  src="{{ asset('tema/assets/images/retails.png') }}" alt="image">
                </div>


            </div>
        </div>
    </section>
    <section id="mobile" class="section-padding">
        <div style="border-bottom: 1px dashed #e30e0e;padding-bottom: 30px;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.mobile')</h3>
                </div>
                <div class="col-md-6 wow fadeInLeft">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/mobile.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.mobile')</h6>
                            <ul>
                                <li style="list-style: none;" id="mobiles">@lang('th.mobile_1')</li>
                                <li style="list-style: none;" id="mobiles">@lang('th.mobile_2')</li>
                                <li style="list-style: none;" id="mobiles">@lang('th.mobile_3')</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 text-right wow fadeInRight">
                    <img src="{{ asset('tema/assets/images/mobile-001.png') }}" alt="image">
                </div>
            </div>
        </div>
    </section>
    <section id="online" class="section-padding">
        <div style="border-bottom: 1px dashed #e30e0e;padding-bottom: 30px;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.online')</h3>
                </div>
                <div class="col-md-5 text-right wow fadeInLeft">
                    <img src="{{ asset('tema/assets/images/online-001.png') }}" alt="image">
                </div>
                <div class="col-md-6 wow fadeInRight">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/online.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.online')</h6>
                            <ul>
                                <li style="list-style: none;" id="onlines">@lang('th.online_1')</li>
                                <li style="list-style: none;" id="onlines">@lang('th.online_2')</li>
                                <li style="list-style: none;" id="onlines">@lang('th.online_3')</li>
                                <li style="list-style: none;" id="onlines">@lang('th.online_4')</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section id="casino" class="section-padding">
        <div style="border-bottom: 1px dashed #e30e0e;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.casino')</h3>
                </div>
                <div class="col-md-6 wow fadeInLeft">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/casino.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.casino')</h6>
                            <ul>
                                <li style="list-style: none;" id="casinos">@lang('th.casino_1')</li>
                                <li style="list-style: none;" id="casinos">@lang('th.casino_2')</li>
                                <li style="list-style: none;" id="casinos">@lang('th.casino_3')</li>
                                <li style="list-style: none;" id="casinos">@lang('th.casino_4')</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 text-right wow fadeInRight">
                    <img src="{{ asset('tema/assets/images/casino-001.png') }}" alt="image">
                </div>
            </div>
        </div>
    </section>
    <section id="virtualsport" class="section-padding">
        <div style="border-bottom: 1px dashed #e30e0e;padding-bottom: 30px;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.v_sports')</h3>
                </div>
                <div class="col-md-5 text-right wow fadeInLeft">
                    <img src="{{ asset('tema/assets/images/virtual-sports-001.png') }}" alt="image">
                </div>
                <div class="col-md-6 wow fadeInRight">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/virtual-sports.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.v_sports')</h6>
                            <ul>
                                <li style="list-style: none;" id="virtualsports">@lang('th.v_sports_1')</li>
                                <li style="list-style: none;" id="virtualsports">@lang('th.v_sports_2')</li>
                                <li style="list-style: none;" id="virtualsports">@lang('th.v_sports_3')</li>
                                <li style="list-style: none;" id="virtualsports">@lang('th.v_sports_4')</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section id="adminconsole" class="section-padding">
        <div style="border-bottom: 1px dashed #e30e0e;padding-bottom: 30px;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.a_console')</h3>
                </div>
                <div class="col-md-6 wow fadeInLeft">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/admin-console.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.a_console')</h6>
                            <ul>
                                <li style="list-style: none;" id="adminconsoles">@lang('th.a_console_1')</li>
                                <li style="list-style: none;" id="adminconsoles">@lang('th.a_console_2')</li>
                                <li style="list-style: none;" id="adminconsoles">@lang('th.a_console_3')</li>
                                <li style="list-style: none;" id="adminconsoles">@lang('th.a_console_4')</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 text-right wow fadeInRight">
                    <img src="{{ asset('tema/assets/images/admin-console-001.png') }}" alt="image">
                </div>
            </div>
        </div>
    </section>
    <section id="odds" class="section-padding">
        <div style="padding-bottom: 30px;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.odds')</h3>
                </div>
                <div class="col-md-5 text-right wow fadeInLeft">
                    <img src="{{ asset('tema/assets/images/odds-data-service-001.png') }}" alt="image">
                </div>
                <div class="col-md-6 wow fadeInRight">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/odds-data-service.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.odds')</h6>
                            <ul>
                                <li style="list-style: none;" id="odd">@lang('th.odds_1')</li>
                                <li style="list-style: none;" id="odd">@lang('th.odds_2')</li>
                                <li style="list-style: none;" id="odd">@lang('th.odds_3')</li>
                                <li style="list-style: none;" id="odd">@lang('th.odds_4')</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section style="background-color: #cdcdcd;" id="highest" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow fadeInLeft">
                    <div class="vertical_box">
                        <div class="box-info">
                            <h6>@lang('th.highset_data')</h6>
                            <p>@lang('th.highset_data_1')
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-right wow fadeInRight">
                    <img class="highest_img" src="{{ asset('tema/assets/images/highest.png') }}" alt="image">
                </div>
            </div>
        </div>
    </section>
    <section id="whitelabel" style="padding-top: 90px;" class="section-padding">
        <div style="border-bottom: 1px dashed #e30e0e;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.w_label')</h3>
                </div>
                <div class="col-md-6 wow fadeInLeft">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/white-label.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.w_label')</h6>
                            <ul>
                                <li style="list-style: none;" id="whitelabels">@lang('th.w_label_1')</li>
                                <li style="list-style: none;" id="whitelabels">@lang('th.w_label_2')</li>
                                <li style="list-style: none;" id="whitelabels">@lang('th.w_label_3')</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 text-right wow fadeInRight">
                    <img src="{{ asset('tema/assets/images/white-label-001.png') }}" alt="image">
                </div>
            </div>
        </div>
    </section>
    <section id="freecupon" class="section-padding">
        <div style="border-bottom: 1px dashed #e30e0e;padding-bottom: 30px;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.f_cupon')</h3>
                </div>
                <div class="col-md-6 wow fadeInLeft">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/free-coupon.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.f_cupon')</h6>
                            <ul>
                                <li style="list-style: none;" id="freecupons">@lang('th.f_cupon_1')</li>
                                <li style="list-style: none;" id="freecupons">@lang('th.f_cupon_2')</li>
                                <li style="list-style: none;" id="freecupons">@lang('th.f_cupon_3')</li>
                                <li style="list-style: none;" id="freecupons">@lang('th.f_cupon_4')</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 text-right wow fadeInRight">
                    <img src="{{ asset('tema/assets/images/free-coupon-001.png') }}" alt="image">
                </div>
            </div>
        </div>
    </section>
    <section id="freebet" class="section-padding">
        <div style="border-bottom: 1px dashed #e30e0e;padding-bottom: 30px;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.f_bet')</h3>
                </div>
                <div class="col-md-5 text-right wow fadeInLeft">
                    <img src="{{ asset('tema/assets/images/free-bet-001.png') }}" alt="image">
                </div>
                <div class="col-md-6 wow fadeInRight">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/free-bet.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.f_bet')</h6>
                            <ul>
                                <li style="list-style: none;" id="freebets">@lang('th.f_bet_1')</li>
                                <li style="list-style: none;" id="freebets">@lang('th.f_bet_2')</li>
                                <li style="list-style: none;" id="freebets">@lang('th.f_bet_3')</li>
                                <li style="list-style: none;" id="freebets">@lang('th.f_bet_4')</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section id="cashout" class="section-padding">
        <div style="padding-bottom: 30px;" class="container">
            <div class="row">
                <div id="donen_yazi"  class="col-md-1 text-right wow zoomIn">
                    <h3 id="donen_yazi">@lang('th.cashout')</h3>
                </div>
                <div class="col-md-6 wow fadeInLeft">
                    <div class="vertical_box">
                        <div class="box-icon">
                            <img src="{{ asset('tema/assets/images/icon/cashout.png') }}">
                        </div>
                        <div class="box-info">
                            <h6>@lang('th.cashout')</h6>
                            <ul>
                                <li style="list-style: none;" id="cashouts">@lang('th.cashout_1')</li>
                                <li style="list-style: none;" id="cashouts">@lang('th.cashout_2')</li>
                                <li style="list-style: none;" id="cashouts">@lang('th.cashout_3')</li>
                                <li style="list-style: none;" id="cashouts">@lang('th.cashout_4')</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 text-right wow fadeInRight">
                    <img src="{{ asset('tema/assets/images/cashout-001.png') }}" alt="image">
                </div>
            </div>
        </div>
    </section>
@endsection


