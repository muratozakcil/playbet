@extends('layouts.master')
@section('title','Playbetic')
@section('head')
    <style>
        #check_con
        {
            width: 6%;
        }
        @media only screen and (max-width: 600px) {
            #check_con
            {
                width: 48%;
            }
        }
        .tab {
            overflow: hidden;
        }

        /* Style the buttons inside the tab */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #60c4dd;
            color: white;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            -webkit-animation: fadeEffect 1s;
            animation: fadeEffect 1s;
        }

        /* Fade in tabs */
        @-webkit-keyframes fadeEffect {
            from {opacity: 0;}
            to {opacity: 1;}
        }

        @keyframes fadeEffect {
            from {opacity: 0;}
            to {opacity: 1;}
        }
        /* The container */
        .container-checkbox {
            display: block;
            position: relative;
            padding-left: 27px;
            margin-bottom: 8px;
            cursor: pointer;
            font-size: 13px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .container-checkbox input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        /* Create a custom checkbox */
        .container-checkbox .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 20px;
            width: 20px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container-checkbox:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container-checkbox input:checked ~ .checkmark {
            background-color: #dc3545;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .container-checkbox .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container-checkbox input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container-checkbox .checkmark:after {
            left: 8px;
            top: 4px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

    </style>
@endsection
@section('content')
    <!-- About-Us -->
    <section id="solutions" style="padding: 160px 0"  class="section-padding">
        <div style="margin-bottom: 50px;" class="container">
            <div class="box-info">
                <div class="row">
                    <div class="col-md-6">
                        <h6 style="font-weight: 600;">OUR LOCATION</h6>
                        <p><i class="far fa-map-marked"></i> Game Limited Corradino Heights, Rahal Gdid PLA 3000, Malta</p>
                        <h6 style="font-weight: 600;">CALL US</h6>
                        <p><i class="fa fa-phone"></i> +31 655 94 5895</p>
                    </div>
                    <div class="col-md-6">

                        <h6 style="font-weight: 600;">DROP US A LINE</h6>
                        <p><i class="fa fa-envelope"></i><a href="mailto:sales@playbetic.com"> sales@playbetic.com</a></p>
                        <p><i class="fa fa-envelope"></i><a href="mailto:info@playbetic.com"> info@playbetic.com</a></p>
                    </div>
                </div>

            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="tab">
                    <button class="tablinks" id="defaultOpen" onclick="openCity(event, 'general')">General Enquries</button>
                    <button class="tablinks" onclick="openCity(event, 'sales')">Sales Enquries</button>
                </div>

                <div id="general" class="tabcontent">
                    <section class="section-padding">
                        <div class="container">
                            <div class="contact_form div_zindex white-text">
                                <form method="post" action="{{ route('contact_general') }}" class="js-ajax-form" novalidate="novalidate">
                                    {{ csrf_field() }}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="form-control" name="name" type="text" placeholder="Name Surname" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" name="email" type="email" placeholder="Email" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" name="phone" type="text" placeholder="Phone" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" name="skype" type="text" placeholder="Skype" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" name="whatsapp" type="text" placeholder="Whatsapp" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div style="color: black;" class="col-md-12">
                                        <label>What are you interested in ?</label>
                                        <div class="form-group">
                                            <label class="container-checkbox">Sports Betting
                                                <input type="checkbox" name="interested[]" value="Sports Betting">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Casino
                                                <input type="checkbox" name="interested[]" value="Casino">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Newsletter
                                                <input type="checkbox" name="interested[]" value="Newsletter">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Demo
                                                <input type="checkbox" name="interested[]" value="Demo">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Other
                                                <input type="checkbox" name="interested[]" value="Other">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div style="color: black;" class="col-md-12">
                                        <div class="form-group">
                                            <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>
                                        </div>
                                    </div>
                                    <div style="color: black;" class="col-md-12">
                                        <label>What you want to ask ?</label>
                                        <div class="form-group">
                                            <textarea name="message" rows="5" class="form-control" placeholder="Message"></textarea>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn" value="SEND">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>

                <div id="sales" class="tabcontent">
                    <section class="section-padding">
                        <div class="container">
                            <div class="contact_form div_zindex white-text">
                                <form style="color: black;" class="js-ajax-form" method="post" action="{{ route('contact_sales') }}" novalidate="novalidate">
                                    {{ csrf_field() }}
                                    <label>Please enter your info </label><br>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" name="company" type="text" placeholder="Company" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" name="website" type="text" placeholder="Website" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" name="cperson" type="text" placeholder="Contact Person" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" name="phone_number" type="text" placeholder="Phone Number" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" name="position" type="text" placeholder="Position" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input class="form-control" name="email" type="email" placeholder="Email" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div style="color: black;" class="col-md-12">
                                        <label>What type of solution are you searching for ?</label>
                                        <div class="form-group">
                                            <label class="container-checkbox">White Label
                                                <input type="checkbox" name="solution[]" value="White Label">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">SportsBook
                                                <input type="checkbox" name="solution[]" value="SportsBook">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Ommi Channel
                                                <input type="checkbox" name="solution[]" value="Ommi Channel">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Retail Soution
                                                <input type="checkbox" name="solution[]" value="Retail Soution">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Agent Soution
                                                <input type="checkbox" name="solution[]" value="Agent Soution">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Other
                                                <input type="checkbox" name="solution[]" value="Other">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Which is your target markets ?</label>
                                        <div class="form-group">
                                            <input class="form-control" name="country" type="text" placeholder="Country" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Which is your target markets ?</label>
                                        <div class="form-group">
                                            <input class="form-control" name="language" type="text" placeholder="Languages" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>What license do you have ?</label>
                                        <div class="form-group">
                                            <input class="form-control" name="license" type="text" placeholder="License" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>What is your current GGR ?</label>
                                        <div class="form-group">
                                            <input class="form-control" name="license" type="text" placeholder="Amount" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Please select currencies you're interested in ?</label>
                                        <div class="form-group">
                                            <input class="form-control" name="currencies" type="text" placeholder="Currencies" required="" aria-required="true">
                                        </div>
                                    </div>
                                    <div style="color: black;" class="col-md-12">
                                        <label>SportsBook</label>
                                        <div class="form-group">
                                            <label class="container-checkbox">Live
                                                <input type="checkbox" name="sportsbook[]" value="Live">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">SportsBook
                                                <input type="checkbox" name="sportsbook[]" value="SportsBook">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Virtual Sports
                                                <input type="checkbox" name="sportsbook[]" value="Virtual Sports">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">E-sports
                                                <input type="checkbox" name="sportsbook[]" value="E-sports">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Match Animation
                                                <input type="checkbox" name="sportsbook[]" value="Match Animation">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Statistics center
                                                <input type="checkbox" name="sportsbook[]" value="Statistics center">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Live score
                                                <input type="checkbox" name="sportsbook[]" value="Live score">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container-checkbox">Live streaming
                                                <input type="checkbox" name="sportsbook[]" value="Live streaming">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div style="color: black;" class="col-md-12">
                                        <label>Which casino providers do you want ? </label>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="container-checkbox">Amatic
                                                        <input type="checkbox" name="casino_provid[]" value="Betsoft">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Betsoft
                                                        <input type="checkbox" name="casino_provid[]" value="Betsoft">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">EGT
                                                        <input type="checkbox" name="casino_provid[]" value="EGT">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Endorphina
                                                        <input type="checkbox" name="casino_provid[]" value="Endorphina">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Reeltime
                                                        <input type="checkbox" name="casino_provid[]" value="Reeltime">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Fugaso
                                                        <input type="checkbox" name="casino_provid[]" value="Fugaso">
                                                        <span class="checkmark"></span>
                                                    </label>

                                                </div>
                                                <div class="col-md-4">
                                                    <label class="container-checkbox">Gameart
                                                        <input type="checkbox" name="casino_provid[]" value="Gameart">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Ganapati
                                                        <input type="checkbox" name="casino_provid[]" value="Ganapati">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Habanero
                                                        <input type="checkbox" name="casino_provid[]" value="Habanero">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Slot motion
                                                        <input type="checkbox" name="casino_provid[]" value="Slot motion">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Isoftbet
                                                        <input type="checkbox" name="casino_provid[]" value="Isoftbet">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Join Games
                                                        <input type="checkbox" name="casino_provid[]" value="Join Games">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="container-checkbox">Microgaming
                                                        <input type="checkbox" name="casino_provid[]" value="Microgaming">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Netent
                                                        <input type="checkbox" name="casino_provid[]" value="Netent">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Spinomenal
                                                        <input type="checkbox" name="casino_provid[]" value="Spinomenal">
                                                        <span class="checkmark"></span>
                                                    </label>

                                                    <label class="container-checkbox">Onetouch
                                                        <input type="checkbox" name="casino_provid[]" value="Onetouch">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Pragmatic play
                                                        <input type="checkbox" name="casino_provid[]" value="Pragmatic play">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Quickspin
                                                        <input type="checkbox" name="casino_provid[]" value="Quickspin">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Realtime
                                                        <input type="checkbox" name="casino_provid[]" value="Realtime">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <label class="container-checkbox">Tom horn
                                                        <input type="checkbox" name="casino_provid[]" value="Tom horn">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div style="color: black;" class="col-md-12">
                                        <div class="form-group">
                                            <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>
                                        </div>
                                    </div>
                                    <div style="color: black;" class="col-md-12">
                                        <label>What you want to ask ?</label>
                                        <div class="form-group">
                                            <textarea name="message" rows="5" class="form-control" placeholder="Message"></textarea>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn" value="SEND">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
    ​<script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
        document.getElementById("defaultOpen").click();
    </script>
@endsection


