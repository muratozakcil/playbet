<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $ayarlar['ayar']['site_title'] }}</title>
    <meta name="description" content="{{ $ayarlar['ayar']['site_desc'] }}">
    <meta name="keywords" content="{{ $ayarlar['ayar']['keywords'] }}">
    @include('layouts.partials.head')
    @yield('head')
    <style>
        .select-language {
            position: absolute;
            left: 22px;
            top: 12px;
        }
        .dropdown-menu
        {
            background: #ffffff!important;
            fill: #ffffff!important;
        }
        @media only screen and (max-width: 600px) {
            .select-language {
                position: absolute;
                left: 9px;
                top: 1px;
            }
            .slides {
                background-position: center top;
                background-size: cover;
                position: relative;
                padding: 95px 0 0px;
            }
        }
    </style>
</head>
<body id="home">
<!-- Header -->
<header id="header" class="nav-stacked affix-top" data-spy="affix" data-offset-top="10">
    <!-- Navigation -->
    <nav id="navigation_bar" class="navbar navbar-default">
        <div class="btn-group select-language">

            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript://" aria-expanded="false">
                @if(\Illuminate\Support\Facades\Lang::getLocale() == "en")
                    <img src="{{ asset('tema/assets/images/EN.svg') }}" width="14">

                    <span>English</span>
                    <span class="caret"></span>
                @endif
                @if(\Illuminate\Support\Facades\Lang::getLocale() == "de")
                    <img src="{{ asset('tema/assets/images/DE.svg') }}" width="14">&nbsp;
                    <span>Germany</span>
                    <span class="caret"></span>
                @endif
                @if(\Illuminate\Support\Facades\Lang::getLocale() == "tr")
                    <img src="{{ asset('tema/assets/images/TR.svg') }}" width="14">&nbsp;
                    <span>Türkçe</span>
                    <span class="caret"></span>
                @endif


            </a>
            <ul class="dropdown-menu">
                <li class="dropdown-item"><a href="{{ url('lang/en') }}"><img src="{{ asset('tema/assets/images/EN.svg') }}" width="16">&nbsp;<span>ENGLISH</span></a></li>
                <li class="dropdown-item"><a href="{{ url('lang/de') }}"><img src="{{ asset('tema/assets/images/DE.svg') }}" width="16">&nbsp;<span>GERMAN</span></a></li>
                <li class="dropdown-item"><a href="{{ url('lang/tr') }}"><img src="{{ asset('tema/assets/images/TR.svg') }}" width="16">&nbsp;<span>TÜRKÇE </span></a></li>

            </ul>
        </div>
        <div class="container">
            <div class="navbar-header">
                <div class="logo"> <a href="{{ route('anasayfa') }}"><img src="{{ asset('tema/assets/images/logo.png') }}" alt="image"/></a> </div>
                <button id="menu_slide" data-target="#navigation" aria-expanded="false" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navigation">
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('anasayfa') }}" class="js-target-scroll">@lang('th.home')</a></li>
                    <li><a href="#sportsbook" class="js-target-scroll">@lang('th.sportsbook')</a></li>
                    <li><a href="{{ route('partner_payment') }}" class="js-target-scroll">@lang('th.payment_partners')</a></li>
                    <li><a href="{{ route('partner_gaming') }}" class="js-target-scroll">@lang('th.gaming_partners')</a></li>
                    <li><a href="{{ route('contact') }}" class="js-target-scroll">@lang('th.contact')</a></li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- Navigation end -->

</header>
<!-- /Header -->
@yield('content')
<!-- /Latest-News -->

@include('layouts.partials.footer')
@yield('footer')
</body>
</html>
