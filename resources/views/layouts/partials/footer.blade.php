
<footer id="footer" class="secondary-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <ul class="footer_links">
                    <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i>playbetic</a></li>
                    <li><a href="mailto:sales@playbetic.com"><i class="fa fa-envelope" aria-hidden="true"></i>sales@playbetic.com</a></li>
                    <li><a href="mailto:info@playbetic.com"><i class="fa fa-envelope" aria-hidden="true"></i>info@playbetic.com</a></li>
                    <li><a href="tel:+31 655 94 5895"><i class="fa fa-whatsapp" aria-hidden="true"></i>+31 655 94 5895</a></li>
                </ul>
            </div>
            <div style="padding: 50px;" class="col-md-6 col-sm-6">
                <p>Playbetic  is an information technology company that provides solutions for all your ideas and We as a whole, are ingenious specialists working together as a team for the best user experience  .  Our company is focused on creating premium software for betting shops. We offer a comprehensive range of products and services which are needed to operate betting shops within regulated and unregulated markets.</p>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-push-6">
                    <div class="follow_us">
                        <ul>
                            <li><a href="https://www.facebook.com/playbetic"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.twitter.com/playbetc"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.instagram.com/playbetic/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="back-top" class="back-top">
    <a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a>
</div>
<script src="{{ asset('tema/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('tema/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('tema/assets/js/interface.js') }}"></script>
<script src="{{ asset('tema/assets/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('tema/assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('tema/assets/js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('tema/assets/switcher/js/switcher.js') }}"></script>
<script src="{{ asset('tema/assets/js/jssor.slider-28.0.0.min.js') }}"></script>

<script type="text/javascript">
    window.jssor_1_slider_init = function() {

        var jssor_1_SlideshowTransitions = [
            {$Duration:800,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
            {$Duration:800,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
        ];

        var jssor_1_options = {
            $AutoPlay: 1,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Orientation: 2,
                $NoDrag: true
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*#region responsive code begin*/

        var MAX_WIDTH = 1600;

        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*#endregion responsive code end*/
    };
</script>
<style>
    /*jssor slider loading skin spin css*/
    .jssorl-009-spin img {
        animation-name: jssorl-009-spin;
        animation-duration: 1.6s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
    }

    @keyframes jssorl-009-spin {
        from { transform: rotate(0deg); }
        to { transform: rotate(360deg); }
    }

    .jssora061 {display:block;position:absolute;cursor:pointer;}
    .jssora061 .a {fill:none;stroke:#fff;stroke-width:1400;stroke-linecap:round;}
    .jssora061:hover {opacity:.8;}
    .jssora061.jssora061dn {opacity:.5;}
    .jssora061.jssora061ds {opacity:.3;pointer-events:none;}


</style>
<script type="text/javascript">jssor_1_slider_init();
</script>
