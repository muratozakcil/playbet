<link rel="stylesheet" href="{{ asset('tema/assets/css/bootstrap.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('tema/assets/css/style.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('tema/assets/css/owl.carousel.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('tema/assets/css/owl.transitions.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('tema/assets/css/prettyPhoto.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('tema/assets/css/font-awesome.min.css') }}" >
<link rel="stylesheet" id="switcher-css" type="text/css" href="{{ asset('tema/assets/switcher/css/switcher.css') }}" media="all" />
<link rel="alternate stylesheet" type="text/css" href="{{ asset('tema/assets/switcher/css/red.css') }}" title="red" media="all" data-default-color="true" />
<link rel="alternate stylesheet" type="text/css" href="{{ asset('tema/assets/switcher/css/orange.css') }}" title="orange" media="all" />
<link rel="alternate stylesheet" type="text/css" href="{{ asset('tema/assets/switcher/css/blue.css') }}" title="blue" media="all" />
<link rel="alternate stylesheet" type="text/css" href="{{ asset('tema/assets/switcher/css/pink.css') }}" title="pink" media="all" />
<link rel="alternate stylesheet" type="text/css" href="{{ asset('tema/assets/switcher/css/green.css') }}" title="green" media="all" />
<link rel="alternate stylesheet" type="text/css" href="{{ asset('tema/assets/switcher/css/purple.css') }}" title="purple" media="all" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
<script src="{{ asset('tema/assets/js/wow.min.js') }}"></script>
<script>
    new WOW().init();
</script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
