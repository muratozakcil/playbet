@extends('layouts.master')
@section('title','Playbetic')
@section('content')
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="cube-wrapper">
                <div class="cube-folding">
                    <span class="leaf1"></span>
                    <span class="leaf2"></span>
                    <span class="leaf3"></span>
                    <span class="leaf4"></span>
                </div>
                <span class="loading" data-name="Loading">Loading</span>
            </div>
        </div>
    </div>
    <header class="navigation">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 p-0">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="/">
                            <img src="{{ asset('tema/assets/images/logo-color.png') }}" alt="">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainmenu"
                                aria-controls="mainmenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="mainmenu">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#home">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#about">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sportsbook">Sportsbook</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#virtuals">Virtuals</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#casino">Casino</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#lotto">Lotto</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#blog">Blog</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#contact">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section id="get-intuch" class="get-intuch">
        <div class="background"></div>
        <div class="round-shape1">
            <img src="{{ asset('tema/assets/images/round-shape1.png') }}" alt="">
        </div>
        <div class="round-shape2">
            <img src="{{ asset('tema/assets/images/round-shape1.png') }}" alt="">
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-10 col-xl-8 text-center">
                    <div class="content">
                        <h2 class="title">Success</h2>
                        <p class="subtitle">Your message has been sent successfully. You will be contacted shortly.</p>
                        <a href="/" class="mybtn1 link">Go Home <i class="fa fa-home"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


