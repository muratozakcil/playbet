-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 10 Şub 2020, 13:46:29
-- Sunucu sürümü: 10.1.37-MariaDB
-- PHP Sürümü: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `betting_england`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `admin_kullanici`
--

CREATE TABLE `admin_kullanici` (
  `id` int(10) UNSIGNED NOT NULL,
  `adsoyad` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sifre` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aktif_mi` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `olusturma_tarihi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `guncelleme_tarihi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `silinme_tarihi` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `admin_kullanici`
--

INSERT INTO `admin_kullanici` (`id`, `adsoyad`, `email`, `sifre`, `aktif_mi`, `remember_token`, `olusturma_tarihi`, `guncelleme_tarihi`, `silinme_tarihi`) VALUES
(1, 'playbetic', 'info@playbetic.com', '$2y$10$PRhFXAlA9dNCF4Tqpt7FneopRaFuNIIi9YM/FG7f/Xp1pMx3R8j6G', 1, NULL, '2020-01-21 17:47:39', '2020-01-21 17:47:39', NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `ayarlar`
--

CREATE TABLE `ayarlar` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_title` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_desc` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_telefon` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_email` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_skype` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hakkimizda` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `ayarlar`
--

INSERT INTO `ayarlar` (`id`, `site_title`, `site_desc`, `keywords`, `site_telefon`, `site_email`, `site_skype`, `hakkimizda`, `created_at`, `updated_at`) VALUES
(1, 'Playbetic', 'Playbetic', 'Playbetic', '+44 7723 524901', 'info@playbetic.com', 'playbetic', '\r\nWe provide sports betting and gaming platform to over 15 partners in 4 continents. The adjustability of the product makes it powerful in both regulated and unregulated markets. Playbetic Software puts all the experience gained as IT division of European sportsbook – Scandic Bookmakers into the gaming platform to make sure that it’s tailored exactly to operator’s needs. Our 50+ team of developers, support staff and IT experts ensures the highest quality of services thanks to knowledge and experience gained since 2005. Thanks to close cooperation with our customers over those years, we continued to improve our platform to make it fully suitable for both regulated and traditional gaming markets all around the world thanks to having the GLI-19 certificate of integrity.\r\n\r\nOur mission is not to only provide the best software for both e-gaming and retail gaming, but also to share our vast experience with new and existing operators and guide them to success in the very competitive market. To enquiry about our products visit our contact us section and send us a message.', NULL, NULL);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `baslik` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kisa_aciklama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `aciklama` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `resim` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `blog`
--

INSERT INTO `blog` (`id`, `baslik`, `slug`, `kisa_aciklama`, `keywords`, `aciklama`, `resim`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test', 'asdasd', 'asdasd', '<p>asdadasd</p>', '1579807873.png', '2020-01-23 16:31:13', '2020-01-23 16:31:13');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_01_21_170927_create_admin_kullanici_table', 1),
(4, '2020_01_21_172958_create_blog_table', 2),
(5, '2020_01_21_173039_create_ayarlar_table', 2),
(6, '2020_01_21_173056_create_slider_table', 2),
(7, '2020_02_01_160306_create_partners_table', 3);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tur` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `partners`
--

INSERT INTO `partners` (`id`, `image`, `tur`, `created_at`, `updated_at`) VALUES
(3, '001.png', 'payment', '2020-02-05 10:29:24', '2020-02-05 10:29:24'),
(4, '002.png', 'payment', '2020-02-05 10:29:24', '2020-02-05 10:29:24'),
(5, '003.png', 'payment', '2020-02-05 10:29:24', '2020-02-05 10:29:24'),
(6, '004.png', 'payment', '2020-02-05 10:29:24', '2020-02-05 10:29:24'),
(7, '005.png', 'payment', '2020-02-05 10:29:24', '2020-02-05 10:29:24'),
(8, '006.png', 'payment', '2020-02-05 10:29:24', '2020-02-05 10:29:24'),
(9, '007.png', 'payment', '2020-02-05 10:29:24', '2020-02-05 10:29:24'),
(10, '008.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(11, '009.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(12, '010.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(13, '011.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(14, '012.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(15, '013.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(16, '014.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(17, '015.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(18, '016.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(19, '017.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(20, '018.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(21, '019.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(22, '020.png', 'payment', '2020-02-05 10:29:25', '2020-02-05 10:29:25'),
(23, 'game-partners-001.png', 'gaming', '2020-02-05 10:29:44', '2020-02-05 10:29:44'),
(24, 'game-partners-002.png', 'gaming', '2020-02-05 10:29:44', '2020-02-05 10:29:44'),
(25, 'game-partners-003.png', 'gaming', '2020-02-05 10:29:44', '2020-02-05 10:29:44'),
(26, 'game-partners--003.png', 'gaming', '2020-02-05 10:29:44', '2020-02-05 10:29:44');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `slider`
--

CREATE TABLE `slider` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sira` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `slider`
--

INSERT INTO `slider` (`id`, `image`, `sira`, `created_at`, `updated_at`) VALUES
(1, '1579807500.jpeg', '1', '2020-01-23 16:25:00', '2020-01-23 16:25:00');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `admin_kullanici`
--
ALTER TABLE `admin_kullanici`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_kullanici_email_unique` (`email`);

--
-- Tablo için indeksler `ayarlar`
--
ALTER TABLE `ayarlar`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Tablo için indeksler `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `admin_kullanici`
--
ALTER TABLE `admin_kullanici`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `ayarlar`
--
ALTER TABLE `ayarlar`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Tablo için AUTO_INCREMENT değeri `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Tablo için AUTO_INCREMENT değeri `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
