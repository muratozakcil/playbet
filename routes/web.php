<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('ajax_regen_captcha', function(){
    return captcha_img();
});
Route::get('/success', 'HomeController@success')->name('success');
Route::get('/gaming-partner', 'HomeController@partner_gaming')->name('partner_gaming');
Route::get('/payment-partner', 'HomeController@partner_payment')->name('partner_payment');
Route::get('/', 'HomeController@index')->name('anasayfa');
Route::get('/blog/{slug}', 'HomeController@blog_detay')->name('blog_detay');
Route::get('/contact', 'HomeController@iletisim')->name('contact');
Route::get('/lang/{lang}', 'LangController@index');
Route::post('/contact-sales', 'HomeController@iletisim_sales')->name('contact_sales');
Route::post('/contact-general', 'HomeController@iletisim_general')->name('contact_general');

Route::group(['prefix' => 'yonetim', 'namespace' => 'yonetim'], function (){
    Route::redirect('/','/yonetim/oturumac');
    Route::match(['get','post'],'/oturumac', 'UserController@yonetim')->name('login');
    Route::group(['middleware' => 'auth'], function (){
        Route::get('/', 'HomeController@index')->name('yonetim.home');
        Route::get('/ayarlar', 'AyarController@index')->name('yonetim.ayar');
        Route::post('/logout', 'UserController@logout')->name('yonetim.logout');
        //yonetim/bloglar
        Route::group(['prefix' => 'blog'], function (){
            Route::match(['get', 'post'], '/', 'BlogController@index')->name('yonetim.blog');
            Route::get('/yeni', 'BlogController@form')->name('yonetim.blog.yeni');
            Route::get('/duzenle/{id}', 'BlogController@form')->name('yonetim.blog.duzenle');
            Route::get('/delete/{id}', 'BlogController@delete')->name('yonetim.blog.delete');
            Route::post('/kaydet/{id?}', 'BlogController@kaydet')->name('yonetim.blog.kaydet');

        });
        //yonetim/slider
        Route::group(['prefix' => 'slider'], function (){
            Route::match(['get', 'post'], '/', 'SliderController@index')->name('yonetim.slider');
            Route::get('/yeni', 'SliderController@form')->name('yonetim.slider.yeni');
            Route::get('/delete/{id}', 'SliderController@delete')->name('yonetim.slider.delete');
            Route::post('/kaydet/{id?}', 'SliderController@kaydet')->name('yonetim.slider.kaydet');

        });
        //yonetim/galeri
        Route::group(['prefix' => 'partners'], function (){
            Route::get('/partners', 'PartnersController@index')->name('yonetim.partners');
            Route::get('/delete/{id}', 'PartnersController@delete')->name('yonetim.partners.delete');
            Route::post('/kaydet', 'PartnersController@kaydet')->name('yonetim.partners.kaydet');

        });
        //ayar-post-data
        Route::post('/genelayarlar', 'AyarController@genelayarlar')->name('yonetim.ayar.genelayarlar');

    });

});
//Clear Cache facade value:
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/clear-config', function () {
    $exitCode = Artisan::call('config:clear');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/cache-config', function () {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Cache facade value cleared</h1>';
});
Route::get('/optimize', function () {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});
Route::get('/route-cache', function () {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});
Route::get('/route-clear', function () {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});
Route::get('/view-clear', function () {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});
